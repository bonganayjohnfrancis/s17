//[SECTION] Array Methods

let fruits = ['Apple', 'Guava', 'Orange'];
console.log(fruits); 
fruits.push('Guyabano', 'Rambutan', 'Dalandan', 'Papaya'); 

   	//2. REMOVE elements inside an array -> pop(), this will allow us to 'remove the last element' inside the array AND returns the removed element from the array.
   			//SYNTAX: array.pop(); 
    //console.log(fruits) //BEFORE
   	let resultNgPop = fruits.pop(); 
   	console.log(resultNgPop); 
   	//take note: when selecting a mutator to perform, ALWAYS keep in mind the 'use case'.

   	//3. REMOVE/ADD one or more element inside an array. unshift() / shift()

   	  //shift() -> remove an 'element at the beginning' of the array AND returns the removed element.

   	  //SYNTAX: array.shift() 
   	  // let removedElement = fruits.shift();
   	  // console.log(removedElement); 

   	  //unshift() -> ADD one or more element at the beginning of an array.
   	    //SYNTAX: array.unshift(ADD MULTIPLE VALUES)

   	   fruits.unshift('Lime', 'Banana', 'Dragon Fruit'); 



   let lastIndexCount = fruits.length - 1; 
   //console.log(fruits[lastIndexCount]); //display the last element
   console.log(fruits); //AFTER

   let names = ['Juan', 'John', 'Doe', 'Eric', 'Mark']; 
   //let removeElement = names.shift(); 
   //names.push(names.shift()); 

   //cons: you wont be able to reuse the element that was removed. 

   console.log(names); //collection of data
   
   //splice() -> changes the contents of an array by removing or replacing the existing elements. 

   //SYNTAX: array.splice(start Index, #ElementsToDel, [OPTIONAL])
     //[OPTIONAL] -> the items that will be inserted to replace the elements starting from start index. (item0 -> itemN)

   //identify the start index
   console.log(names.indexOf('John')); //1

   let spliceResult = names.splice(0, 4, 'Marie', 'Lisa', 'Kathryn', 'Thonie'); 
   console.log(spliceResult); 
   console.log(names); 

   let pets = ['cat', 'monkey', 'dog', 'sheep',0, 20, 39, 134];
   pets.sort(); //arrange the contents in alphanumeric order, in inceasing order 
   let numbers = [0, 1000, 20, 36, 425]; 
   numbers.reverse()
   //console.log(numbers); 

   //console.log(pets);

   //reverse() -> rearranges the elements in reverse order.

   let countries = ['PH', 'US', 'GER', 'FR', 'CHI'];
   countries.reverse(); 
   // console.log(countries); 


   //NEW TASK: Create logic that will organize the data sets in decreasing order. 
   
   //left => right 
   //visualize:
     // 80 > 78 = 1  
     // 78, 98
   let grades = [80, 78, 98, 39, 68, 72, 100]; 
   //by default sort() -> it organizes/sorts the elements inside an array in ascending order. 

   //possible solution 1: to change the behavior or the outcome of sort(), we can create a method that will allow us to assess the value of the elements
    
   //lets try to change of the behavior of our current output, keep in mind that the script is this module is running on a single 'thread'. 

   //Threading 

   //Its all about understanding the syntax and parameters of a method. 

   // sort() SYNTAX:

   //1st:  arary.sort() //functionless syntax

   //2nd: array.sort(compareFn) //   

      //parameters:
         //compareFn (OPTONAL) => this will define the sort order. 

            //compareFn (a, b) 
               // a -> the first element for comparison
               // b -> the second element for comparison


   //increasing order values
   //yes -> this is an example of bubble sorting. Bubble sorting as the simplest sorting algorithm that works by repeatedly swapping adjacent elements in a data structure who are originally placed in the wrong order.
   let gradesInInc = () => grades.sort( function(elementA, elementB) {
   		//create a control structure that will assess and compare the 2 values.
         //visualization of the workflow: 
            // [2, 13, 3, 4]
            //=> [2, 3, 13, 4]
            //=> [2, 3, 4, 13, 13] //no switch
   		if (elementA > elementB) return 1;
   		if (elementA < elementB) return -1; 
   		return 0;   
   }); 


   //decreasing order
   let gradesInDecOrder = () => grades.sort( function(numA, numB) {
   		//create a control structures to compare the values of the collection
   		if(numA > numB) return -9; 
   		if(numA < numB) return 10; 
   		return 0; 
   }); 

  console.log(gradesInInc());
  console.log(gradesInDecOrder()); 
  //by changing the structure on how repackaged the return of the sort function, we were able to change the flow of the Thread. meaning that the logic will only run or be compiled if the function is invoked or called out. 

  //lets try another example. 

  //create a another data set of number collection. 
  let ratings = [78.1, 85.7, 68.2, 92.1, 100, 98.0]; 

  //[68.2, 78.1, 85.7, 92.1, 98.0, 100]

  //create a a function that will arrange the data sets in increasing order. 

  // [leastAmout -> greatestAmount]

  // im gonna be using an arrow based function
  let sortData = () => ratings.sort( function(a, b) {
      //create a logic that will identify how the positions of each element will be arrange to get the desired outcome.
      if (a < b) return -5; //so that a will be placed before b, the return should be 'any' value < 0. 
      if (a > b) return 8;//if you want to place 'b' before 'a', the return should be any value >0.
      return 0; //if neither of the conditions were met, it means NO switching will be done.
  })

  console.log(sortData()); 

//[SECTION] Non-Mutators

  //toString() -> this will allow us to to return an array as a string separated by commas. 

     let places = ['Manila', 'Tondo', 'Q.C.', 'Makati', 'Laguna']; 

     //SYNTAX: array.toString(); 
     console.log(places); 

     let stringedArray = places.toString(); 
     console.log(stringedArray); //to confirm change in the data type.
     console.log(typeof stringedArray); 

   //slice() -> this will allow us to get a specific portion of an array. 

   const zoo = ['ant', 'bison', 'camel', 'dog', 'duck', 'lion']; 

   //SYNTAX: 
   // 1st:  array.slice();
   // 2nd:  array.slice(start, end)

         //start (OPTIONAL)
           //-> INDEX COUNT (zero-based index at which to start the extraction)

         //end (OPTIONAL)
            //-> ZERO based index before which to end the extraction of elements. (slice method extracts up to the end but NOT including the 'end').

   //slice() => cut/remove NO adding elements. 
   //splice() => cut/remove/add elements in an array
   // console.log(zoo); 
   // console.log(zoo.slice()); 
   // console.log(zoo.slice(0, 3));
   // //WHAT IF I ONLY WANT TO GET 'camel'? 
   // console.log(zoo.slice(2,3)); 
   // //WHAT IF I ONLY WANT TO GET 'dog' and 'duck'?
   // console.log(zoo.slice(3,5)); 

   //concat() -> combines 2 arrays and returns the combined result. 

   //SYNTAX: arrayA.concat(arrayB); 

   let newArray = zoo.concat(places);
   console.log(newArray); //both arrays were combined with each other. 

