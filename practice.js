let grades = [98, 97, 90, 98, 96, 100, 89, 78, 68, 92]; 

    //imagine that you are a teacher who is trying to identify if a student is qualified for high honors or highest honors.

    //=> highest honor => no grade below 90. 

    // let isHighHonor = grades.some( function(grade) {
    //     return grade <= 89; 
    // }); 

    // console.log(isHighHonor); //true || false?

    //filter() -> this is used to return a new array that contains element which meets the given condition. 

    //Use and Benefits: This method is useful when filtering or whenever you want to acquire/select/retrieve items that will pass a requirement.

    //Syntax: 

       // let/const newArray = arrayName.filter( function(arrayElement) {
       //    //conditional statement of requirements
       // })

    //NOTE: Also several array iteration can also perform this task and provide the necessary/intended result. 

    //create a filter logic that will only retrieve the grades that are above 90. 

    let gradesAbove90 = grades.filter( function(grade) {
       //statement of requirements
       //when is it required to wrap a statement inside the scope of a parenthesis?
       //parenthsis in JS can be used to denote scope of a statement or condition. 
       //to 'group' statements. => to better undertsand and to have better perspective on how component are evaluated
       return (grade >= 90) && (grade <= 95);
    });

    console.log(gradesAbove90); 
